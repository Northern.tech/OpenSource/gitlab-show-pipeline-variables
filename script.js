document.addEventListener('animationstart', function(event){
    if (event.animationName == 'nodeInserted'){
        link = event.srcElement;
        var match = link.href.match("^https://gitlab.com/(.*?)(?:/-)?/pipelines/([0-9]+)$")
        if (match) {
            var repo = match[1]
            var pipeline = match[2]
            a = document.createElement("a");
            a.appendChild(document.createTextNode("Vars"));
            a.className = "gitlab-show-pipeline-variables"
            a.title = "Pipeline variables";
            a.href = "https://gitlab.com/api/v4/projects/" + encodeURIComponent(repo) + "/pipelines/" + pipeline + "/variables";
            sup = document.createElement("sup");
            sup.appendChild(a);
            link.parentNode.insertBefore(sup, link.nextSibling);
            link.parentNode.insertBefore(document.createTextNode(" "), link.nextSibling);
        }
    }
}, true);
