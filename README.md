Show variables for Gitlab pipelines
===================================

Firefox extension which makes a little button next to Pipeline links in
Gitlab. Clicking on it leads to a page listing the variables that the pipeline
was submitted with. This information is not available in the Gitlab UI, but it
is available in the API, which is what the extension uses.

License
-------

Licensed under Apache License version 2.0. See the [`LICENSE`](LICENSE) file for
details.
